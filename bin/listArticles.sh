#!/bin/bash

week=`date '+%-V'`
#week=$((`date '+%V'`-1))

cd `dirname $0`/../work
wget "https://www.april.org/rp/?nid=1&cite=-1&order=0&q=&count=100"
mv index.html\?* index.html

grep -o "\(href=\"https://[^\"]*\|<td>IRC(.*)<\/td>\)" index.html |
grep -o "\(https://[^\"]*\|IRC(.*)\)" >> articles$week.txt

aEnlever=`grep -o "enlever([[:digit:]]*)" index.html | grep -o "[[:digit:]]*"`
for i in $aEnlever
do
  wget https://www.april.org/rp/enlever_article.php?id=$i
  rm enlever_article.php\?id\=*
done
