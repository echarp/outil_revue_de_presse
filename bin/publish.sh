#!/bin/bash

cd `dirname $0`/../work

week=`date '+%-V'`
# Use the following in tokens.sh to override
#week=$((`date '+%V'`-1))
source tokens.sh
dir=`echo $week`revueDePresse
week=`echo $week|sed "s|^0\(.*\)|\1|g"`
[ ! -d $dir ] && mkdir $dir && echo Generating $dir
cd $dir
echo $dir

#                                             _ _ 
#                             _ __ ___   __ _(_) |
#                            | '_ ` _ \ / _` | | |
#                            | | | | | | (_| | | |
#                            |_| |_| |_|\__,_|_|_|
echo Generating and sending mail to rp@april.org
echo "From: echarp <echarpentier@april.org>
To: april@april.org
Subject: Revue de presse pour la semaine $week (relecture)
http://www.april.org/revue-de-presse-de-lapril-pour-la-semaine-X-de-lannee-2015
" > mail.txt

w3m http://www.april.org/revue-de-presse-par-courriel-sous-forme-de-liste -dump \
  | sed -n "/Revue de presse pour la semaine X/, /^http:\/\/linuxfr.org/ p" \
  | sed "s|\(Cette revue de presse sur Internet fait partie du travail de veille mené par\)|En podcast sur https:\/\/www.april.org\/category\/type-de-publication\/decryptualite\n\n\1|g" \
  | sed "/^« /,/ »$/ s/^/> /g" \
  | sed "s/^> « /> /g" \
  | sed "s/ »$//g" \
  | sed "s|/$\n|/|g" \
  | sed "/ • Éditer/, / • Cloner/ d" \
  | sed "s/Et aussi:/\nEt aussi:/g" \
  | sed "s/* \"\(.*\)\":http/* \1\nhttp/g" \
  | sed "s/* \"/* /g" \
  | sed "s/\":http/\n  http/g" \
  | sed "/ • Éditer/, / • Cloner/ d" \
  >> mail.txt

sed -i "s/semaine X/semaine $week/g" mail.txt
sed -i "s/semaine-X/semaine-$week/g" mail.txt

PS3="Que faire avec le mail? "
select choice in "Envoyer" "Rien"
do
  echo $choice
  case $choice in
    Envoyer )
      #scp mail.txt echarp.org:/tmp && ssh -t echarp.org mutt -H /tmp/mail.txt
      mutt -H mail.txt
      break;;
    Rien )
      break;;
  esac
done

#                            _                        _ 
#                         __| |_ __ _   _ _ __   __ _| |
#                        / _` | '__| | | | '_ \ / _` | |
#                       | (_| | |  | |_| | |_) | (_| | |
#                        \__,_|_|   \__,_| .__/ \__,_|_|
#                                        |_|            
w3m http://www.april.org/revue-de-presse-sommaire-actualite -dump -cols 1024 \
  | sed -n "/^La <a href/,/<\/ul>/ p" \
  > intro.txt

w3m http://www.april.org/revue-de-presse-actualite -dump -cols 1024 \
  | sed -n "/<h3>/,/^\[/ p" \
  | sed 's/^* "\(.*\)":\(.*\)$/• <a href="\2">\1<\/a>/g' \
  | sed "s/^\(Et aussi:\)$/\n\1/g" \
  | head -n -1 \
  > body.txt

rm drupal.txt
cat intro.txt >> drupal.txt
cat body.txt >> drupal.txt
sed -i "s|<h3>Sommaire|En <a href=\"https://www.april.org/category/type-de-publication/decryptualite\">podcast</a>.\n\n<h3>Sommaire|g" drupal.txt
echo "
&lt;h4&gt;Note&lt;/h4&gt;

&lt;em&gt;Les articles de presse utilisent souvent le terme « Open Source » au lieu de Logiciel Libre. Le terme Logiciel Libre étant plus précis et renforçant l'importance des libertés, il est utilisé par l'April dans sa communication et ses actions. Cependant, dans la revue de presse nous avons choisi de ne pas modifier les termes employés par l'auteur de l'article original. Même chose concernant l'emploi du terme « Linux » qui est généralement utilisé dans les articles pour parler du système d'exploitation libre GNU/Linux.&lt;/em&gt;" >> drupal.txt

sed -i "s/semaine X/semaine $week/g" drupal.txt

PS3="Que faire avec la revue de presse? "
select choice in "Editer" "Prévoir" "Arrêter"
do
  echo $choice
  case $choice in
    Editer )
      vi drupal.txt intro.txt body.txt
      ;;
    Prévoir )
      cp ../../template/actualite.html .
      cat drupal.txt >> actualite.html
      cat ../../template/actualiteEnd.html >> actualite.html
      sed -i "s/semaine X/semaine $week/g" actualite.html
      sed -i "s|\$drupal_token|$actualite_token|g" actualite.html
      w3m actualite.html
      firefox https://www.april.org/fr/les-brouillons

      cd ../..
      publishLinuxFr.sh
      break;;
    Arrêter )
      break;;
  esac
done

#demandePublication.sh
