#!/bin/bash

cd `dirname $0`/../work

echo Vous devez vous logguer avec w3m, par exemple avec:
echo w3m http://april.org
echo w3m http://linuxfr.org

echo
echo En premier, le token pour les articles individuels
var=`w3m -dump_source http://www.april.org/fr/node/add/revue-de-presse| \
  grep form_token| \
  grep edit-revue-de-presse-node-form-form-token| \
  grep -o value=\".*\"| \
  sed -n 's|value=\"\(.*\)\"|\1|p'`

sed -i "s/article_token=.*/article_token=$var/" tokens.sh

echo
echo Ensuite, le token pour la RP hebdomadaire
var=`w3m -dump_source http://www.april.org/fr/node/add/actualite| \
  grep form_token| \
  grep edit-actualite-node-form-form-token| \
  grep -o value=\".*\"| \
  sed -n 's|value=\"\(.*\)\"|\1|p'`

sed -i "s/actualite_token=.*/actualite_token=$var/" tokens.sh

exit

echo
echo Finalement, le token pour linuxfr
var=`w3m -dump_source http://linuxfr.org/news/nouveau| \
  grep -o authenticity_token.*| \
  grep -o 'value=".*"'| \
  sed -n 's|value="\(.*\)"|\1|p'| \
  head -1`

sed -i "s/depeche_token=.*/depeche_token=$var/" tokens.sh
