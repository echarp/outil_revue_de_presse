#!/bin/bash

href=$1
url=$1
dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && cd .. && pwd)"

# shellcheck source=../work/tokens.sh
source "$dir/work/tokens.sh"

cd "$(dirname "$0")/../work" || exit
mkdir -p tmp && cd tmp || exit

cp ../../template/rp.html .
# rm article.txt
# w3m "$url" -dump > article.txt
rm article.html
wget -cN "$href" -O article.html

function extract() {
  # This is the destination variable
  declare -n ret=$1
  # Get the related pattern
  declare -n pattern=$1_pattern

  # Get out if variable already filled
  [[ $ret ]] && return 0

  ret="$(sed -nr "s|^\\s*$pattern$|\\1|p" article.html)"
  [[ ! $ret ]] && ret="$(sed -nr "s|^\\s$pattern.*|\\1|p" article.html)"
  [[ ! $ret ]] && ret="$(sed -nr "s|.*$pattern.*|\\1|p" article.html)"

  [[ ! $ret ]] && ret="$(sed -nr "s|.*${default_patterns[$1]}.*|\\1|p" article.html | head -n 1)"

  if [[ $ret ]]
  then
    if [[ $ret =~ "&#" || $ret =~ "eacute" ]]
    then
      ret=$(echo "$ret" | recode html)
    fi

    # Replacing some characters
    ret=${ret//&#039;/\"}
    ret=${ret//&quot;/\'}
    # Deleting extra spaces
    ret=${ret//&nbsp;}
    ret=${ret// / }
    ret=${ret//« /«}
    ret=${ret// »/»}
    ret=${ret// \:/:}
    ret=${ret// \!/!}
    ret=${ret// \?/?}
    ret=${ret//’/\'}

    echo "$1: $ret"
  else
    echo "Nothing found for \"$1\" with pattern: $pattern, or default pattern: ${default_patterns[$1]}"
  fi
}

# Declaring variables that extractions will output to
site=
title=
author=
date=
abstract=

declare -A default_patterns=(
  [site]='<meta property="og:site_name" content="(.*)".*>'
  [title]='<title.*>\s*(.*)\s*</title>'
  [author]='"author":\{"@type":"Person","name":"(.*)"\}'
  [date]='"datePublished":"([-0-9]*)T'
  [featured_image]='src=.(http.[?&-_\.:/a-zA-Z0-9]*\....)"'
  [abstract]='<meta name=".escription" content="(.*)".*>'
)

# shellcheck source=/dev/null
# shellcheck source=patterns.sh
source "$dir/bin/patterns.sh" "$href"

extract site
extract title
extract author
extract date
extract featured_image
extract abstract

[[ $date ]] && date=$(echo "$date" | sed -r "s/\\b./\\L&/g" | head -n 1)
[[ $date == '' ]] && echo "No date found, which one to use: " && read -r date
[[ $date == '' ]] && echo 'Sorry, a date is required!!'
[[ $date ]] && date2=$(date -d "$date" +'%Y-%m-%d')
[[ $date2 ]] || date2=$(date -d "$date" +'%Y-%m-%d')
[[ $date2 ]] && date=$(date -d "$date" +'%A %_d %B %Y')

sed -i "s|\$site|$site|g" rp.html
sed -i "s|\$title|[$site] $title|g" rp.html
sed -i "s|\$author|$author|g" rp.html
sed -i "s|\$date2|$date2|g" rp.html
sed -i "s|\$date|$date|g" rp.html
sed -i "s|\$abstract|$abstract|g" rp.html
sed -i "s|\$url|$url|g" rp.html
# sed "/\$article/c $article" rp.html
sed -i "s|\$article||g" rp.html

sed -i "s|\$drupal_token|$article_token|g" rp.html

# Try to prefill some tags based on the article text
IFS=$'\n'
echo
echo -n "Tags found: "
grep "taxonomy\\[10\\]" rp.html|grep -o "/>.*$"|grep -o "\\w.*" | while IFS= read -r i
do
  if grep -qoi "$i" article.html
  then
    echo -n "$i " && sed -i "s| />$i| checked=\"true\"/>$i|" rp.html
  fi
done
echo
echo

sed -i "s|\\([«]\\)\\s|\\1|g" rp.html
sed -i "s|\\s\\([\\?!:»]\\)|\\1|g" rp.html

w3m rp.html
